package com.remindotron.remindotron;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;

/**
 * Created by Andrew Duvernet on 12/8/2015.
 */

public class ReminderNotification extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(this.getApplicationContext() , Settings.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent ,0 );

        Notification mNotify = new Notification.Builder(this)
                .setContentTitle("Remindotron")
                .setContentText("Time to get to work!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setSound(sound)
                .build();

        mNM.notify( 1 , mNotify);

    }
}
