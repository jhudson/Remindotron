package com.remindotron.remindotron;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

import java.util.Calendar;

/**
 * Application to open database, and begin migrations and creations, etc
 */

public class DBApplication extends Application{

    @Override
    public void onCreate(){
        super.onCreate();
        FlowManager.init(this);

        this.initialCreate();

    }


    /*
     * For creating our data when the database is created or recreated
     */
    private void initialCreate(){

        if (LogTable.contains("Downloaded App!")) {

            /*
             * Creates initial class for our Number of Times Logged"
             */

            TimesLoggedTable timesLogged = new TimesLoggedTable();
            timesLogged.name = TimesLoggedTable.TIMESLOGGED;
            timesLogged.timesLogged = 0;
            timesLogged.save();

            /*
             * Start of creating our achievements
             */

            //TODO Talk to Damon about getting a list of the achievements he's created

            AchievementTable dayOneAchievement = new AchievementTable("One Day Achievement", false);
            dayOneAchievement.save();

            AchievementTable daySevenAchievement = new AchievementTable("7 Days Achievement", false);
            daySevenAchievement.save();

            AchievementTable fourteenDaysAchievement = new AchievementTable("14 Days Achievement", false);
            fourteenDaysAchievement.save();

            AchievementTable thirtyDayAchievement = new AchievementTable("30 Days Achievement", false);
            thirtyDayAchievement.save();

            AchievementTable ninetyAchievement = new AchievementTable("90 Days Achievement", false);
            ninetyAchievement.save();

            AchievementTable oneEightyAchievement = new AchievementTable("180 Days Achievement", false);
            oneEightyAchievement.save();

            AchievementTable threeSixtyFiveAchievement = new AchievementTable("365 Days Achievement", false);
            threeSixtyFiveAchievement.save();


            Calendar rightNow = Calendar.getInstance();
            LogTable newLog = new LogTable("Downloaded App!", rightNow.get(Calendar.MONTH)+1, rightNow.get(Calendar.DAY_OF_MONTH));
            newLog.save();
        }
    }
}
