package com.remindotron.remindotron;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainPageFragment extends Fragment {

    protected RecyclerView mRecyclerView;
    protected LogAdapter mLogAdapter;
    protected List logList;
    protected View view;


    public MainPageFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /*
         * TODO
         * Still need to make this page dynamic.  So when the button is pressed the log
         * is added and the button disappears.
         *
         */

        view = inflater.inflate(R.layout.fragment_main_page, container, false);

        Achievements.runAll();

        final Button button = (Button) view.findViewById(R.id.logButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                List logs = LogTable.getList();
                Calendar rightNow = Calendar.getInstance();
                boolean found = false;

                for (int i = 0; i < logs.size(); i++){
                    LogTable currentLog = ((LogTable)logs.get(i));
                    if (currentLog.title.equals(TimesLoggedTable.LOGGED)
                            && currentLog.dayOfMonth == rightNow.get(Calendar.DAY_OF_MONTH)
                            && currentLog.month == rightNow.get(Calendar.MONTH)+1) {
                        found = true;
                    }
                }
                if (!found){
                    TimesLoggedTable.logData(TimesLoggedTable.TIMESLOGGED);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.alert_dialog);
                    builder.show();
                }
            }
        });

        /*
         * This part creates the recycler view loaded up with all of the logs
         */

        //Creates the List and Loads the list up
        logList = LogTable.getList();

        mRecyclerView = (RecyclerView)view.findViewById(R.id.logList);
        mLogAdapter = new LogAdapter();
        mRecyclerView.setAdapter(mLogAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }

    public class LogAdapter extends RecyclerView.Adapter<ViewHolder>{

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            View logView = inflater.inflate(R.layout.log_item, parent, false);

            return new ViewHolder(logView);
        }
        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            TextView textView = viewHolder.textView;
            LogTable currentLog = (LogTable)logList.get((logList.size()-1) - position);
            textView.setText(currentLog.title + "    " + currentLog.month + "/" + currentLog.dayOfMonth);
        }

        public int getItemCount(){
            return logList.size();
        }


    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;

        public ViewHolder(View logView) {
            super(logView);

            textView = (TextView)logView.findViewById(R.id.logItem);
        }
    }
}
