package com.remindotron.remindotron;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Update;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.sql.Time;
import java.util.Calendar;

/**
 * Table that contains our logged data.
 *
 * Making this a table on it's own so if we have a chance for tracking
 * multiple activities we can add them here.
 */

@Table(databaseName = RemindotronDB.NAME)
public class TimesLoggedTable extends BaseModel {

    public static final String LOGGED = "Logged Activity";

    public static final String TIMESLOGGED = "Number of Times Logged";

    @Column
    @PrimaryKey(autoincrement = true) //not really used but if ever needed easier to have it than not
    long id;

    @Column
    String name;

    @Column
    int timesLogged;

    public TimesLoggedTable(){
        super();
    }

    public TimesLoggedTable(String name){
        super();
        this.name = name;
        this.timesLogged = 0;
    }

    /*
     *  Returns the DataStored object of our dataName
     */

    public static TimesLoggedTable getData(String dataName){
        return new Select().from(TimesLoggedTable.class)
                .where(Condition.column(TimesLoggedTable$Table.NAME).is(dataName)).querySingle();
    }

    /*
     *  Increments our timesLogged value
     */

    public static void logData(String dataName){
        Calendar rightNow = Calendar.getInstance();

        int newValue = getData(dataName).timesLogged + 1;
        Where update = new Update<>(TimesLoggedTable.class)
                .set(Condition.column(TimesLoggedTable$Table.TIMESLOGGED).eq(newValue))
                .where(Condition.column(TimesLoggedTable$Table.NAME).eq(dataName));
        update.queryClose();

        LogTable newLog = new LogTable(LOGGED, rightNow.get(Calendar.MONTH)+1, rightNow.get(Calendar.DAY_OF_MONTH));
        newLog.save();

    }

}
