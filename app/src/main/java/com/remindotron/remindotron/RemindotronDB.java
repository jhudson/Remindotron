package com.remindotron.remindotron;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Our Database for Remindotron
 */

@Database(name = RemindotronDB.NAME, version = RemindotronDB.VERSION)
public class RemindotronDB {
    public static final String NAME = "MainDatabase";
    public static final int VERSION = 1;
}

