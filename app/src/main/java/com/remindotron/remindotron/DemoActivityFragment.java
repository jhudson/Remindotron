package com.remindotron.remindotron;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class DemoActivityFragment extends Fragment {

    private View view;

    public DemoActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_demo, container, false);

        Button demoButton = (Button)view.findViewById(R.id.demoLog);
        demoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TimesLoggedTable.logData(TimesLoggedTable.TIMESLOGGED);
                TextView timesLogged = (TextView)view.findViewById(R.id.demoTimesLogged);
                timesLogged.setText("" + TimesLoggedTable.getData(TimesLoggedTable.TIMESLOGGED).timesLogged);
            }
        });

        return view;
    }
}
