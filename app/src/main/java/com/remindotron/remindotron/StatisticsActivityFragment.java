package com.remindotron.remindotron;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class StatisticsActivityFragment extends Fragment {

    public StatisticsActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);

        TextView textNumberLogged = (TextView)view.findViewById(R.id.currentLogged);
        String textNumberLoggedString = "Number of times Logged:   "  + TimesLoggedTable.getData(TimesLoggedTable.TIMESLOGGED).timesLogged;
        textNumberLogged.setText(textNumberLoggedString);

        TextView lastLogged = (TextView)view.findViewById(R.id.lastLogged);
        List logs = LogTable.getList();
        String lastLoggedString = "Last Time Logged: ";
        for (int i = logs.size()-1; i > 0; i--){
            LogTable currentLog = ((LogTable)logs.get(i));
            if (currentLog.title.equals(TimesLoggedTable.LOGGED)){
                lastLoggedString = lastLoggedString + currentLog.month + "/" + currentLog.dayOfMonth;
                break;
            }
        }
        lastLogged.setText(lastLoggedString);




        //TODO Implement currentStreak and bestStreak

        TextView textCurrentStreak = (TextView)view.findViewById(R.id.currentStreak);
        String textCurrentStreakString = "Current Streak:  " + 0;
        textCurrentStreak.setText(textCurrentStreakString);

        TextView textBestStreak = (TextView)view.findViewById(R.id.bestStreak);
        String textBestStreakString = "Best Streak:  " + 0;
        textBestStreak.setText(textBestStreakString);




        return view;
    }
}
