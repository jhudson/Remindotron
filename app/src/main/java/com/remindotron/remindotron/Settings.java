package com.remindotron.remindotron;

import android.app.AlarmManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.app.TimePickerDialog;
import android.widget.Toast;


import junit.framework.Test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Calendar;

/**
 * Created by Andrew Duvernet on 12/7/2015.
 */

public class Settings extends AppCompatActivity {

    Button setAlarm;
    DateFormat fmtHourAndMinute=DateFormat.getTimeInstance();
    TextView hourAndMinuteLabel;
    // hourAndMinuteLabel=(TextView)findViewById(R.id.hourAndMinuteLabel);
    Calendar hourAndMinute=Calendar.getInstance();
    private int hour;
    private int minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);



        setAlarm = (Button) findViewById(R.id.setUpAlarm);
        hourAndMinuteLabel = (TextView)findViewById(R.id.hourAndMinuteLabel);



        final TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay,
                                  int minute) {
                hourAndMinute.set(Calendar.HOUR_OF_DAY, hourOfDay);
                hourAndMinute.set(Calendar.MINUTE, minute);
                updateLabel();
            }
        };

        setAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new TimePickerDialog(Settings.this,
                        t,
                        hourAndMinute.get(Calendar.HOUR_OF_DAY),
                        hourAndMinute.get(Calendar.MINUTE),
                        true).show();


            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.main_page) {
            Intent intent = new Intent(this, MainPage.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.stats) {
            Intent intent = new Intent(this, StatisticsActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.achievements) {
            Intent intent = new Intent(this, Achievements_Activity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.demo) {
            Intent intent = new Intent(this, DemoActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateLabel() {
        hourAndMinuteLabel.setText(fmtHourAndMinute.format(hourAndMinute.getTime()));
    }

}
