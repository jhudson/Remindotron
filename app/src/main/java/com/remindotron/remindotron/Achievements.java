package com.remindotron.remindotron;

/**
 * Created by damondantin on 11/29/15.
 */
public class Achievements {

    public Achievements(){
        dayOne_Achievement();
        daySeven_Achievement();
        dayFourteen_Achievement();
        dayThirty_Achievement();
        dayNinety_Achievement();
        dayOneEighty_Achievement();
        dayThreeSixty_Achievement();
    }
    // >= 1
    public static void dayOne_Achievement(){
        if(TimesLoggedTable.getData("Number of Times Logged").timesLogged >= 1){
            AchievementTable.setAchieved("One Day Achievement" );
        }
    }
    // >= 7
    public static void daySeven_Achievement(){
        if(TimesLoggedTable.getData("Number of Times Logged").timesLogged >= 7){
            AchievementTable.setAchieved("7 Days Achievement" );
        }
    }
    // >= 14
    public static void dayFourteen_Achievement(){
        if(TimesLoggedTable.getData("Number of Times Logged").timesLogged >= 14){
            AchievementTable.setAchieved("14 Days Achievement" );
        }
    }
    // >= 30
    public static void dayThirty_Achievement(){
        if(TimesLoggedTable.getData("Number of Times Logged").timesLogged >= 30){
            AchievementTable.setAchieved("30 Days Achievement");
        }
    }
    // >= 90
    public static void dayNinety_Achievement(){
        if(TimesLoggedTable.getData("Number of Times Logged").timesLogged >= 90){
            AchievementTable.setAchieved("90 Days Achievement");
        }
    }
    // >= 180
    public static void dayOneEighty_Achievement(){
        if(TimesLoggedTable.getData("Number of Times Logged").timesLogged >= 180){
            AchievementTable.setAchieved("180 Days Achievement");
        }
    }
    // >= 365
    public static void dayThreeSixty_Achievement(){
        if(TimesLoggedTable.getData("Number of Times Logged").timesLogged >= 365) {
            AchievementTable.setAchieved("365 Days Achievement");
        }
    }

    public static void runAll(){
        dayOne_Achievement();
        daySeven_Achievement();
        dayFourteen_Achievement();
        dayThirty_Achievement();
        dayNinety_Achievement();
        dayOneEighty_Achievement();
        dayThreeSixty_Achievement();

    }

}
