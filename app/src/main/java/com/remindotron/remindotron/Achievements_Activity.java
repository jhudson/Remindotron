package com.remindotron.remindotron;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Achievements_Activity extends AppCompatActivity {
    Achievements achievements;
    ArrayList<String> listItems = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    ListView listview;


//Toast.makeText(getApplicationContext(), pieString[position], Toast.LENGTH_SHORT).show();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievements_);

        Achievements.runAll();

        achievements = new Achievements();

        /*
         * Hey Damon
         * I took the intent portion out of this part as it was running
         * fairly slow when starting the achievement activity. Leaving it
         * in in case you want to work on the optimization of it later.
         *
         * -John
         */

        //String message = "The message is a NO-GO..... BUllshit";
        //Intent intent = getIntent();
        //if(intent != null){
        //    if(intent.hasExtra("com.talkingandroid.MESSAGE")){
        //        message = intent.getStringExtra("com.talkingandroid.MESSAGE");
        //    }
        //    else if
        //        (intent.hasExtra(Intent.EXTRA_TEXT)){
        //           message = intent.getStringExtra(Intent.EXTRA_TEXT);
        //        }
        //    }

        //Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();


        if (AchievementTable.hasAchieved("One Day Achievement") == true) {
            listItems.add("One Day Achievement");
        }
        if (AchievementTable.hasAchieved("7 Days Achievement") == true) {
            listItems.add("7 Day Achievement");
        }
        if (AchievementTable.hasAchieved("14 Days Achievement") == true) {
            listItems.add("14 Day Achievement");
        }
        if (AchievementTable.hasAchieved("30 Days Achievement") == true) {
            listItems.add("30 Day Achievement");
        }
        if (AchievementTable.hasAchieved("90 Days Achievement") == true) {
            listItems.add("90 Day Achievement");
        }
        if (AchievementTable.hasAchieved("180 Days Achievement") == true) {
            listItems.add("180 Day Achievement");
        }
        if (AchievementTable.hasAchieved("365 Days Achievement") == true) {
            listItems.add("365 Day Achievement");
        }
        adapter = new ArrayAdapter<String>(this, R.layout.text_achievement_list, listItems);
        listview = (ListView) findViewById(android.R.id.list);
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.main_page) {
            Intent intent = new Intent(this, MainPage.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.stats) {
            Intent intent = new Intent(this, StatisticsActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.achievements) {
            Intent intent = new Intent(this, Achievements_Activity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.demo) {
            Intent intent = new Intent(this, DemoActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
