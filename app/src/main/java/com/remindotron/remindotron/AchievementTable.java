package com.remindotron.remindotron;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Update;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.sql.Time;
import java.util.Calendar;

/**
 * Table that contains our achievement data
 */

@Table(databaseName = RemindotronDB.NAME)
public class AchievementTable extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true) //not really used but if ever needed easier to have it than not
    long id;

    @Column
    String achievementName;

    @Column
    boolean hasAchieved;

    public AchievementTable(){
        super();
    }

    public AchievementTable(String achievementName, boolean hasAchieved){
        super();
        this.achievementName = achievementName;
        this.hasAchieved = hasAchieved;
    }

    /*
     *  This method returns if an achievement has been achieved yet
     *  true   if it has been achieved
     *  false  if it has not been
     */

    public static boolean hasAchieved(String achievementName){
        return new Select().from(AchievementTable.class)
                .where(Condition.column(AchievementTable$Table.ACHIEVEMENTNAME).is(achievementName)).querySingle().hasAchieved;
    }

    /*
     *  Marks an achievement as completed
     *
     *  If called on an achievement that has already been achieved it will have no effect
     */

    public static void setAchieved(String achievementName){
        Where update = new Update<>(AchievementTable.class).set(Condition.column(AchievementTable$Table.HASACHIEVED).eq(true)).where(Condition.column(AchievementTable$Table.ACHIEVEMENTNAME).eq(achievementName));
        update.queryClose();

        //Makes sure achievements log hasn't already been creating
        if (LogTable.contains(achievementName)) {
            Calendar rightNow = Calendar.getInstance();
            LogTable newLog = new LogTable(achievementName, rightNow.get(Calendar.MONTH)+1, rightNow.get(Calendar.DAY_OF_MONTH));
            newLog.save();
        }

    }



}