package com.remindotron.remindotron;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * A table that keeps track of each activity that was logged.
 *
 * Stores the date the activity was recorded along with a description
 * Also keeps track of when achievements were achieved
 */

@Table(databaseName = RemindotronDB.NAME)
public class LogTable extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    String title;

    @Column
    int month;

    @Column
    int dayOfMonth;

    public LogTable() {
        super();
    }

    public LogTable(String title, int month, int dayOfMonth) {
        super();
        this.title = title;
        this.month = month;
        this.dayOfMonth = dayOfMonth;
    }

    /*
     * Returns a Log given the Logs primary key.
     *
     * For use in getting the list of Logs through a loop.  Get the last 5
     * logs that were logged etc.
     *
     */

    public static LogTable getLog(long primaryKey) {
        return new Select().from(LogTable.class)
                .where(Condition.column(LogTable$Table.ID).is(primaryKey)).querySingle();
    }

    public static List getList(){
        return new Select()
                .from(LogTable.class)
                .where()
                .orderBy(true, LogTable$Table.ID)
                .queryList();
    }

    public static boolean contains(String name){
        boolean result = false;
        LogTable log = new Select().from(LogTable.class).where(Condition.column(LogTable$Table.TITLE).is(name)).querySingle();
        if (log == null){
            result = true;
        }
        return result;
    }

}